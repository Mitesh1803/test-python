from flask import Flask, jsonify, request
from pymongo import MongoClient
from bson import ObjectId

app = Flask(__name__)

# MongoDB connection
client = MongoClient("mongodb://mongo:27017/")
db = client.flaskdb

@app.route('/create', methods=['POST'])
def create():
    data = request.json
    result = db.users.insert_one(data)
    return jsonify(message="User created", _id=str(result.inserted_id)), 201


@app.route('/read', methods=['GET'])
def read():
    users = list(db.users.find())
    for user in users:
        user["_id"] = str(user["_id"])
    return jsonify(users)


@app.route('/update/<id>', methods=['PUT'])
def update(id):
    db.users.update_one({'_id': ObjectId(id)}, {"$set": request.json})
    return jsonify(message="User updated")

@app.route('/delete/<id>', methods=['DELETE'])
def delete(id):
    db.users.delete_one({'_id': ObjectId(id)})
    return jsonify(message="User deleted")

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False)
