import pytest
import requests

BASE_URL = "http://localhost:5000"

@pytest.fixture(scope="module")
def test_user():
    user_data = {"name": "Test User", "age": 30}
    requests.post(f"{BASE_URL}/create", json=user_data)
    users = requests.get(f"{BASE_URL}/read").json()
    user_id = next(user['_id'] for user in users if user['name'] == "Test User" and user['age'] == 30)
    yield user_id
    requests.delete(f"{BASE_URL}/delete/" + user_id)  # Cleanup after tests

def test_create_user():
    response = requests.post(f"{BASE_URL}/create", json={"name": "New User", "age": 25})
    assert response.status_code == 201

def test_read_users():
    response = requests.get(f"{BASE_URL}/read")
    assert response.status_code == 200
    assert len(response.json()) >= 1

def test_update_user(test_user):
    new_data = {"name": "Updated User", "age": 35}
    response = requests.put(f"{BASE_URL}/update/" + test_user, json=new_data)
    assert response.status_code == 200

def test_delete_user(test_user):
    response = requests.delete(f"{BASE_URL}/delete/" + test_user)
    assert response.status_code == 200
